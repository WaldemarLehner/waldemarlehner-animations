import { makeProject } from '@motion-canvas/core';
import scene from './no-movement-example.scene?scene';

export default makeProject({
  scenes: [scene],
});
