import { makeScene2D, Circle, SVG, Grid, Txt, Line } from '@motion-canvas/2d';
import {
  Vector2,
  createComputed,
  createRef,
  createSignal,
  linear,
  waitFor,
} from '@motion-canvas/core';

const svgString = `<svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 24 24"><path fill="currentColor" d="m5 21l-1-1l8-18l8 18l-1 1l-7-3z"/></svg>`;

export default makeScene2D(function* (scene) {
  const svgRef = createRef<SVG>();

  const time = createSignal(0);
  const orangeAngle = createSignal(-20);
  const timeString = createComputed(
    () =>
      `t = ${time().toLocaleString(undefined, {
        maximumSignificantDigits: 2,
        maximumFractionDigits: 2,
        minimumSignificantDigits: 2,
        minimumFractionDigits: 2,
      })}s`,
    time
  );

  yield scene.add(<Grid width={'100%'} height={'100%'} spacing={50} />);

  scene.add(
    <SVG ref={svgRef} svg={svgString} position={[-250, 150]} size={100} fill={'red'} zIndex={1} />
  );
  scene.add(
    <SVG ref={svgRef} svg={svgString} position={[250, -150]} size={100} fill={'blue'} zIndex={1} />
  );
  scene.add(<Txt text={timeString} x={400} y={200} fontSize={50} fill={'red'} />);

  scene.add(
    <Circle
      stroke={'red'}
      lineWidth={10}
      position={[-250, 150]}
      size={() => (time() < 0 ? 0 : time() * 1200)}
    />
  );

  scene.add(
    <Line
      lineWidth={5}
      stroke={'green'}
      points={[
        [-250, 150],
        [250, -150],
      ]}
      endArrow
      startArrow
      zIndex={2}
      arrowSize={20}
    />
  );
  scene.add(
    <Line
      lineWidth={5}
      stroke={'orange'}
      points={[
        [-250, 150],
        () =>
          Vector2.fromDegrees(orangeAngle())
            .mul(time() * 600)
            .add([-250, 150]),
      ]}
      endArrow
      startArrow
      zIndex={2}
      arrowSize={20}
    />
  );

  yield* time(1.6, 1.6, linear);
  yield* time(0.97, 2);
  yield* orangeAngle(-31, 1);
  yield* time(0, 1);
  yield* waitFor(0.5);
});
