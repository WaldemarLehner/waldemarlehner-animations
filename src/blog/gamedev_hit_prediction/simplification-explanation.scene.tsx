import { makeScene2D, Circle, SVG, Grid, View2D, Txt } from '@motion-canvas/2d';
import {
  Vector2,
  all,
  createRef,
  easeInQuad,
  linear,
  useRandom,
  waitFor,
} from '@motion-canvas/core';

const svgString = `<svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 24 24"><path fill="currentColor" d="m5 21l-1-1l8-18l8 18l-1 1l-7-3z"/></svg>`;

function* spawnProjectile(scene: View2D, directionDeg: number, speed: number, ttl: number) {
  const projectile = createRef<Circle>();
  scene.add(<Circle fill={'#000'} ref={projectile} size={50} position={0} />);
  const targetPos = Vector2.fromDegrees(directionDeg).mul(speed * ttl);
  yield* all(
    projectile().position(targetPos, ttl, linear),
    projectile().opacity(0, ttl, easeInQuad)
  );
  projectile().remove();
}

function* spawnProjectileAbstraction(scene: View2D, speed: number, ttl: number) {
  const circle = createRef<Circle>();
  scene.add(<Circle stroke={'red'} lineWidth={30} size={0} ref={circle} />);
  yield* all(circle().size(speed * ttl * 2, ttl, linear), circle().opacity(0, ttl, easeInQuad));
  circle().remove();
}

export default makeScene2D(function* (scene) {
  const svgRef = createRef<SVG>();
  const gridRef = createRef<Grid>();

  yield scene.add(<Grid width={'100%'} height={'100%'} spacing={200} ref={gridRef} />);
  scene.add(
    <SVG ref={svgRef} svg={svgString} position={[0, 0]} size={150} fill={'red'} zIndex={1} />
  );

  for (let i = 0; i < 14; i++) {
    const dirs = i > 10 ? [] : useRandom().floatArray(3 + 2 * i, -360, 360);
    for (const dir of dirs) {
      yield spawnProjectile(scene, dir, 500, 1);
    }

    if (i > 6) {
      yield spawnProjectileAbstraction(scene, 500, 1);
    }

    yield* waitFor(0.5);
  }
  yield* waitFor(2);
});
