import { makeProject } from '@motion-canvas/core';
import scene from './target-movement-example.scene?scene';

export default makeProject({
  scenes: [scene],
});
