import { makeScene2D, Circle, SVG, Grid, Txt, Line } from '@motion-canvas/2d';
import {
  Vector2,
  createComputed,
  createRef,
  createSignal,
  linear,
  waitFor,
} from '@motion-canvas/core';

const svgString = `<svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 24 24"><path fill="currentColor" d="m5 21l-1-1l8-18l8 18l-1 1l-7-3z"/></svg>`;

export default makeScene2D(function* (scene) {
  const ownShipRef = createRef<SVG>();
  const enemyShipRef = createRef<SVG>();

  const lineOpacity = createSignal(1);

  const time = createSignal(0);
  const orangeAngle = createSignal(-20);
  const timeString = createComputed(
    () =>
      `t = ${time().toLocaleString(undefined, {
        maximumSignificantDigits: 2,
        maximumFractionDigits: 2,
        minimumSignificantDigits: 2,
        minimumFractionDigits: 2,
      })}s`,
    time
  );

  yield scene.add(<Grid width={'100%'} height={'100%'} spacing={50} />);

  scene.add(
    <SVG
      ref={ownShipRef}
      svg={svgString}
      position={[-250, 150]}
      size={100}
      fill={'red'}
      zIndex={1}
    />
  );
  scene.add(
    <SVG
      ref={enemyShipRef}
      svg={svgString}
      position={() =>
        Vector2.fromDegrees(-100 - 90)
          .mul(time() * 500)
          .add([300, -200])
      }
      size={100}
      fill={'blue'}
      rotation={-100}
      zIndex={1}
    />
  );
  scene.add(<Txt text={timeString} x={400} y={200} fontSize={50} fill={'red'} />);

  scene.add(
    <Circle
      stroke={'red'}
      lineWidth={10}
      position={[-250, 150]}
      size={() => (time() < 0 ? 0 : time() * 1200)}
    />
  );

  scene.add(
    <Line
      lineWidth={5}
      stroke={'green'}
      points={[[-250, 150], enemyShipRef().position]}
      endArrow
      startArrow
      zIndex={2}
      opacity={lineOpacity}
      arrowSize={20}
    />
  );
  scene.add(
    <Line
      lineWidth={5}
      stroke={'orange'}
      points={[
        [-250, 150],
        () =>
          Vector2.fromDegrees(orangeAngle())
            .mul(time() * 600)
            .add([-250, 150]),
      ]}
      endArrow
      startArrow
      opacity={lineOpacity}
      zIndex={2}
      arrowSize={20}
    />
  );

  yield* time(1.0, 1.0, linear);
  yield* time(0.632, 1);
  yield* orangeAngle(-50.9, 1);

  const targetRef = createRef<SVG>();

  scene.add(
    <SVG
      size={0}
      opacity={0}
      zIndex={10}
      position={[-10, -145]}
      rotation={45}
      fill={'red'}
      svg={`<svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 24 24"><path fill="currentColor" d="M22.08 11.04h-2V4h-7.03V2h-2.01v2H4v7.04H2v2.01h2v7.03h7.04v2h2.01v-2h7.03v-7.03h2zm-4.01 7.03h-5.02v-2.01h-2.01v2.01H6v-5.02h2.03v-2.01H6V6h5.04v2.03h2.01V6h5.02v5.04h-2.01v2.01h2.01zm-5.02-6.02a1 1 0 0 1-1 1c-.55 0-1.01-.45-1.01-1s.46-1.01 1.01-1.01s1 .46 1 1.01"/></svg>`}
      ref={targetRef}
    />
  );
  yield targetRef().size(70, 1);
  yield targetRef().opacity(1, 1);
  yield lineOpacity(0, 1);
  yield ownShipRef().rotation(30, 1);

  yield* time(0, 1);
  yield* waitFor(0.5);

  const bulletRef = createRef<Circle>();
  scene.add(
    <Circle
      ref={bulletRef}
      fill={'#000'}
      size={20}
      position={() =>
        Vector2.fromDegrees(-50.5)
          .mul(time() * 600)
          .add(ownShipRef().position())
      }
    />
  );
  yield* time(0.632, 0.632, linear);
  yield bulletRef().remove();
  yield* waitFor(1);
  yield targetRef().opacity(0, 1);
  yield lineOpacity(1, 1);
  yield ownShipRef().rotation(0, 1);
  yield* time(0, 1);
  yield* waitFor(1);
});
