import { defineConfig } from 'vite';
import motionCanvas from '@motion-canvas/vite-plugin';
import { globSync } from 'glob';

export default defineConfig({
  plugins: [
    motionCanvas({
      project: globSync('./src/**/*.project.ts').map((e) => `./${e}`),
    }),
  ],
  build: {
    rollupOptions: {
      output: {
        dir: './dist',
        entryFileNames: '[name].js',
      },
    },
  },
});
